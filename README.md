![](http://i.imgur.com/5Bqs5zi.png)

React boilerplate thenewboston tutorials.

## Getting started

To get started simply download the repo using the link below. All required files are included.

https://github.com/buckyroberts/React-Boilerplate/archive/master.zip

## Setting up Gulp (optional)

You can also use Gulp to add additional build tasks. To use, follow the instructions below.

Navigate to the root directory and  run the following command:
```
> npm install
```

After modules are installed, you can start watching for SCSS changes using the command:
```
> gulp
```

You can install more modules and configure them in the **gulpfile.js** file as needed.

## Links

## For starting up the localhost server : 
1. Install node from nodejs.org (if not installed already)
2. Install http-server from the npm command on the command prompt : "npm install -g http-server"
3. Now go to the directory with your static files and run command : "http-server"
4. Hit localhost:8080 on the browser.